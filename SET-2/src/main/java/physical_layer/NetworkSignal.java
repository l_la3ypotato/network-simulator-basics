package main.java.physical_layer;

import javafx.scene.chart.LineChart;
import javafx.scene.chart.XYChart;

/**
 *
 * @author: Logan Stanfield
 * @since: 11/22/17
 * @version: 1.0
 */
public class NetworkSignal {

    /**
     * This method takes in a message and converts it to a digital signal.
     * 
     * @param message
     * @param chart 
     */
    public static void drawDigitalSignal(String message, LineChart<?, ?> chart) {
        chart.getData().clear();
        XYChart.Series series = new XYChart.Series();
        long xValue = 0;
        for (int char_index = 0; char_index < message.length(); char_index++) {
            char curr_char = message.charAt(char_index);
            long yValue = 1;
            if (curr_char == '1') {
                yValue = -1;
                series.getData().add(new XYChart.Data<>(xValue, yValue));
                xValue += 5;
                series.getData().add(new XYChart.Data<>(xValue, yValue));
            } else if (curr_char == '0') {
                series.getData().add(new XYChart.Data<>(xValue, yValue));
                xValue += 5;
                series.getData().add(new XYChart.Data<>(xValue, yValue));
            }
        }
        chart.getData().add(series);
    }

    /**
     * This method converts a message to a binary representation and plots it
     * to an analog signal graph.
     * 
     * @param message
     * @param chart
     * @param waveSample 
     */
    public static void drawAnalogSignal(String message, LineChart<?, ?> chart, int waveSample) {
        chart.getData().clear();
        XYChart.Series series = new XYChart.Series();
        int length = message.length() * 360;
        for (int char_index = 0; char_index < message.length(); char_index++) {
            char curr_char = message.charAt(char_index);
            for (int rIndex = char_index * 360; rIndex <= (char_index + 1) * 360; rIndex += waveSample) {
                double yValue = 0.0;
                double radians = Math.toRadians(rIndex);
                if (curr_char == '1') {
                    yValue = Math.sin(radians);
                }
                series.getData().add(new XYChart.Data<Number, Number>(radians, yValue));
            }
        }
        chart.getData().add(series);
    }
}
