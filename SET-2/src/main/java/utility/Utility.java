package main.java.utility;

/**
 * This class contains various utility methods that are often referenced by other
 * classes. The two that remain to be unused are customSin() and roundTo().
 * 
 * @author Logan Stanfield
 * @since 11/22/17
 * @version 1.0
 */
public class Utility {

    /**
     * 
     * @param str: The string to be converted to binary.
     * @return The return string in its binary representation.
     */
    public static String convertStringToBinary(String str) {
        // Holdes the byte conversion of the bytes array.
        byte[] bytesArray = str.getBytes();
        
        StringBuilder binaryRepresentation = new StringBuilder();
        // Iterate through each character in the byte array.
        for (byte b : bytesArray) {
            int val = b;
            for (int i = 0; i < 8; i++) {
                // Append necessary 0's
                binaryRepresentation.append((val & 128) == 0 ? 0 : 1);
                // Shift the bits to the left by 1 and assign to variable val.
                val <<= 1;
            }
        }
        return binaryRepresentation.toString();
    }

    /**
     * This function takes in a hex string and returns the binary representation.
     * 
     * @param hex: The passed in hexadecimal values.
     * @return :
     */
    public static String hexToBinary(String hex) {
        long tempValue = Long.parseLong(hex, 16);
        String binary = Long.toBinaryString(tempValue);
        return binary;
    }
    
    /**
     * This function takes in an angle and returns the rounded sine value. This
     * was used because Java does not give the exact value of sine.
     * 
     * @param theta: Angle to be passed in.
     * @return: The value given by the rounded sine function.
     */
    public static double customSin(double theta) {
        double x = roundTo(Math.sin(theta), 10);
        return x;
    }

    /**
     * This value rounds the passed in value to the level of accuracy that is also
     * passed as a parameter.
     * 
     * @param input: The value to be rounded.
     * @param accuracy: The accuracy in which to round the input value to.
     * @return 
     */
    public static double roundTo(double input, int accuracy) {
        double accuracyModifier = Math.pow(10, accuracy);
        double retValue = accuracyModifier * input;
        retValue = Math.round(retValue);
        retValue /= accuracyModifier;
        return retValue;
    }
    
    /**
     * This method adds left padding to a string.
     * 
     * @param str: The passed in string to add padding to.
     * @param numPadding: The number of values to pad with.
     * @param padChar: The padded character.
     * @return The original string, but padded with the specified character and 
     *    amount.
     */
    public static String addPadding(String str, int numPadding, char padChar) {
        String retString = "";
        for (int i = 0; i < numPadding; i++) {
            retString += Character.toString(padChar);
        }
        return retString += str;
    }
}
