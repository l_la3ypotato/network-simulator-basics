package main.java.program;

import com.jfoenix.controls.JFXButton;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.animation.AnimationTimer;
import javafx.animation.ParallelTransition;
import javafx.animation.PathTransition;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Polyline;
import javafx.scene.shape.Rectangle;
import javafx.util.Duration;
import main.java.data_link_layer.ethernet_frame.EthernetFrame;
import main.java.physical_layer.NetworkSignal;
import main.java.utility.Utility;

/**
 * This class is the FXML document controller for assignment 2. It contains many
 * of the various dependency injections and the general code for how the
 * animation works.
 *
 * @author Logan Stanfield
 * @since 10/14/17
 * @version 1.0
 */
public class Assignment2Controller implements Initializable {

    // <editor-fold desc="Class variables">
    private AnimationTimer animationTimer;
    private final double initialXBounds = 232.0;
    private final double redBallXBounds = 787.0;
    private final double blueBallXBounds = 592.0;
    private final double r1Bounds = 400.0;
    private final double r2Bounds = 605.0;
    private final Color defaultRectangleColor = Color.web("#f9f6b6");
    private final Color reachedRectangleColor = Color.ALICEBLUE;
    private PathTransition ackTransition;
    private ParallelTransition ballTransition;
    private boolean animationStarted = false;
    private String message;
    private String ethernetFrame;
    private boolean paused;
    private boolean h1Reached = false;
    private boolean r1Reached = false;
    private boolean r2Reached = false;
    private final String sourceMAC = "D792680FC864";
    private final String destMAC = "E6F7D259A03B";
    // </editor-fold>

    // <editor-fold desc="Java-FX injection variables.">
    @FXML
    private Circle blueBall;
    @FXML
    private Circle redBall;
    @FXML
    private Rectangle rect1;
    @FXML
    private Rectangle rect2;
    @FXML
    private Rectangle rect3;
    @FXML
    private Rectangle rect4;
    @FXML
    private Rectangle rect5;
    @FXML
    private Rectangle rect6;
    @FXML
    private Rectangle rect7;
    @FXML
    private Rectangle rect8;
    @FXML
    private Rectangle rect9;
    @FXML
    private Rectangle rect10;
    @FXML
    private Rectangle rect11;
    @FXML
    private Rectangle rect12;
    @FXML
    private Rectangle rect13;
    @FXML
    private Rectangle rect14;
    @FXML
    private Rectangle rect15;
    @FXML
    private Rectangle rect16;
    @FXML
    private Rectangle rect17;
    @FXML
    private Rectangle rect18;
    @FXML
    private Rectangle rect19;
    @FXML
    private Rectangle rect20;
    @FXML
    private Rectangle rect21;
    @FXML
    private JFXButton playButton;
    @FXML
    private JFXButton pauseButton;
    @FXML
    private JFXButton resetButton;
    @FXML
    private TextField messageField;
    @FXML
    private Label warningLabel;
    @FXML
    private JFXButton sendButton;
    @FXML
    private Label messageReceivedBlue;
    @FXML
    private Label messageReceivedRed;
    @FXML
    private NumberAxis yAxis;
    @FXML
    private NumberAxis xAxis;
    @FXML
    private LineChart<?, ?> signalChart;
    @FXML
    private Label schemeLabel;
    @FXML
    private TextArea displayArea;
    @FXML
    private Circle ackCircle;
    @FXML
    private Label ackReceivedLbl;
    @FXML
    private Label hostMAC2Addr;
    @FXML
    private Label hostMAC1Addr;
    @FXML
    private Label handshakeLbl;
    // </editor-fold>
    
    // <editor-fold desc="Initialization">
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        xAxis.setAutoRanging(true);
        yAxis.setAutoRanging(true);
        signalChart.setCreateSymbols(false);
        signalChart.setStyle("CHART_COLOR_1: #4CAF50");
    }
    
    // </editor-fold>

    // <editor-fold desc="Helper methods">
    /**
     * This function begins the animation process.
     */
    public void moveBall() {
        animationStarted = true;

        // Red ball polyline coordinate map.
        Polyline redPolyline = new Polyline();
        redPolyline.getPoints().addAll(new Double[]{
            0.0, 0.0,
            0.0, 285.0,
            555.0, 285.0,
            555.0, 30.0
        });

        // Blue ball polyline coordinate map.
        Polyline bluePolyline = new Polyline();
        bluePolyline.getPoints().addAll(new Double[]{
            0.0, 0.0,
            0.0, 315.0,
            200.0, 315.0,
            240.0, 140.0,
            360.0, 140.0,
            360.0, -110.0
        });

        // ACK Circle polyline.
        Polyline ackPolyline = new Polyline();
        ackPolyline.getPoints().addAll(new Double[]{
            0.0, 0.0,
            -175.0, 0.0
        });

        // Creates a path transition for ACK notification Circle.
        ackTransition = new PathTransition(Duration.seconds(4), ackPolyline, ackCircle);
        ackTransition.setCycleCount(2);
        ackTransition.setAutoReverse(true);

        // Start the animation timer.
        animationTimer = new AnimationTimer() {
            @Override
            public void handle(long now) {
                double redBallX, redBallY, blueBallX, blueBallY;
                blueBallX = blueBall.getTranslateX() + blueBall.getLayoutX();
                blueBallY = blueBall.getTranslateY() + blueBall.getLayoutY();
                redBallX = redBall.getTranslateX() + redBall.getLayoutX();
                redBallY = redBall.getTranslateY() + redBall.getLayoutY();

                // DEBUG:
                // System.out.println("BLUE: X: " + blueBallX + " Y: " + blueBallY);
                // System.out.println("RED: X: " + redBallX + " Y: " + redBallY);
                if (this.inRectangleBounds(redBallX, redBallY, rect1, initialXBounds)) {
                    rect1.setFill(reachedRectangleColor);
                }
                if (this.inRectangleBounds(redBallX, redBallY, rect2, initialXBounds)) {
                    rect2.setFill(reachedRectangleColor);
                }
                if (this.inRectangleBounds(redBallX, redBallY, rect3, initialXBounds)) {
                    rect3.setFill(reachedRectangleColor);
                }
                if (this.inRectangleBounds(redBallX, redBallY, rect4, initialXBounds)) {
                    rect4.setFill(reachedRectangleColor);
                }
                if (this.inRectangleBounds(redBallX, redBallY, rect5, initialXBounds)) {
                    rect5.setFill(reachedRectangleColor);
                }
                if (this.inRectangleBounds(redBallX, redBallY, rect6, initialXBounds)) {
                    displayArea.setText("Message: " + message + "\n\n" + "Ethernet Frame (binary): " + convertStringToEthernetFrame(message));
                    rect6.setFill(reachedRectangleColor);
                }
                if (this.inRectangleBounds(redBallX, redBallY, rect7, initialXBounds)) {
                    rect7.setFill(reachedRectangleColor);
                    if (!h1Reached) {

                        schemeLabel.setText("NRZ-L Coding Scheme:");
                        ackCircle.setVisible(true);
                        NetworkSignal.drawDigitalSignal(ethernetFrame, signalChart);
                        ackTransition.play();
                        h1Reached = true;
                    }
                }
                if (redBallX > r1Bounds && redBallX < r1Bounds + 10) {
                    if (!r1Reached) {
                        schemeLabel.setText("Binary ASK:");
                        NetworkSignal.drawAnalogSignal(ethernetFrame, signalChart, 60);
                        r1Reached = true;
                        ackReceivedLbl.setVisible(true);
                        handshakeLbl.setVisible(true);
                    }
                }
                if (redBallX > r2Bounds && redBallX < r2Bounds + 10) {
                    if (!r2Reached) {
                        schemeLabel.setText("NRZ-L Coding Scheme:");
                        NetworkSignal.drawDigitalSignal(ethernetFrame, signalChart);
                        r2Reached = true;
                    }
                }
                if (this.inRectangleBounds(blueBallX, blueBallY, rect8, blueBallXBounds)) {
                    rect8.setFill(reachedRectangleColor);
                    messageReceivedBlue.setVisible(true);
                    playButton.setDisable(true);
                    pauseButton.setDisable(true);
                }
                if (this.inRectangleBounds(blueBallX, blueBallY, rect9, blueBallXBounds)) {
                    rect9.setFill(reachedRectangleColor);
                }
                if (this.inRectangleBounds(blueBallX, blueBallY, rect10, blueBallXBounds)) {
                    rect10.setFill(reachedRectangleColor);
                }
                if (this.inRectangleBounds(blueBallX, blueBallY, rect11, blueBallXBounds)) {
                    rect11.setFill(reachedRectangleColor);
                }
                if (this.inRectangleBounds(blueBallX, blueBallY, rect12, blueBallXBounds)) {
                    rect12.setFill(reachedRectangleColor);
                }
                if (this.inRectangleBounds(blueBallX, blueBallY, rect13, blueBallXBounds)) {
                    rect13.setFill(reachedRectangleColor);
                }
                if (this.inRectangleBounds(blueBallX, blueBallY, rect14, blueBallXBounds)) {
                    rect14.setFill(reachedRectangleColor);
                }
                if (this.inRectangleBounds(redBallX, redBallY, rect15, redBallXBounds)) {
                    rect15.setFill(reachedRectangleColor);
                    messageReceivedRed.setVisible(true);
                    playButton.setDisable(true);
                    pauseButton.setDisable(true);
                }
                if (this.inRectangleBounds(redBallX, redBallY, rect16, redBallXBounds)) {
                    rect16.setFill(reachedRectangleColor);
                }
                if (this.inRectangleBounds(redBallX, redBallY, rect17, redBallXBounds)) {
                    rect17.setFill(reachedRectangleColor);
                }
                if (this.inRectangleBounds(redBallX, redBallY, rect18, redBallXBounds)) {
                    rect18.setFill(reachedRectangleColor);
                }
                if (this.inRectangleBounds(redBallX, redBallY, rect19, redBallXBounds)) {
                    rect19.setFill(reachedRectangleColor);
                }
                if (this.inRectangleBounds(redBallX, redBallY, rect20, redBallXBounds)) {
                    displayArea.setText("Message: " + message + "\n\n" + "Ethernet frame received!");
                    rect20.setFill(reachedRectangleColor);
                }
                if (this.inRectangleBounds(redBallX, redBallY, rect21, redBallXBounds)) {
                    signalChart.getData().clear();
                    schemeLabel.setText("Bitstream conversion:");
                    rect21.setFill(reachedRectangleColor);
                }
            }

            // This method checks to see if the ball is in the bounds of the passed in rectangle.
            public boolean inRectangleBounds(double ball_x, double ball_y, Rectangle rect, double xBounds) {
                if (ball_y >= rect.getLayoutY()
                        && ball_x == xBounds
                        && ball_y <= (rect.getLayoutY() + rect.heightProperty().doubleValue())) {
                    return true;
                }

                return false;
            }
        };

        // Start the animation timer.
        animationTimer.start();
        animationTimer.handle(0);

        // Blue ball path transition.
        PathTransition blueBallTransition = new PathTransition(Duration.seconds(20), bluePolyline, blueBall);
        blueBallTransition.setCycleCount(1);

        // Red ball path transition.
        PathTransition redBallTransition = new PathTransition(Duration.seconds(20), redPolyline, redBall);
        redBallTransition.setCycleCount(1);

        // Parallel transition for both red and blue balls. ;)
        ballTransition = new ParallelTransition();
        ballTransition.getChildren().addAll(redBallTransition, blueBallTransition);
        ballTransition.play();

    }

    // This method resets all of the items on the screen.
    private void resetScreen() {
        if (ballTransition != null) {
            ballTransition.stop();
        }
        animationStarted = false;
        rect1.setFill(defaultRectangleColor);
        rect2.setFill(defaultRectangleColor);
        rect3.setFill(defaultRectangleColor);
        rect4.setFill(defaultRectangleColor);
        rect5.setFill(defaultRectangleColor);
        rect6.setFill(defaultRectangleColor);
        rect7.setFill(defaultRectangleColor);
        rect8.setFill(defaultRectangleColor);
        rect9.setFill(defaultRectangleColor);
        rect10.setFill(defaultRectangleColor);
        rect11.setFill(defaultRectangleColor);
        rect12.setFill(defaultRectangleColor);
        rect13.setFill(defaultRectangleColor);
        rect14.setFill(defaultRectangleColor);
        rect15.setFill(defaultRectangleColor);
        rect16.setFill(defaultRectangleColor);
        rect17.setFill(defaultRectangleColor);
        rect18.setFill(defaultRectangleColor);
        rect19.setFill(defaultRectangleColor);
        rect20.setFill(defaultRectangleColor);
        rect21.setFill(defaultRectangleColor);
        blueBall.setTranslateX(0.0);
        blueBall.setTranslateY(0.0);
        redBall.setTranslateX(0.0);
        redBall.setTranslateY(0.0);
        warningLabel.setVisible(false);
        messageField.setDisable(false);
        sendButton.setDisable(false);
        messageReceivedBlue.setVisible(false);
        messageReceivedRed.setVisible(false);
        pauseButton.setDisable(true);
        playButton.setDisable(true);
        messageField.clear();
        signalChart.getData().clear();
        schemeLabel.setText("");
        displayArea.clear();
        ackCircle.setVisible(false);
        if (ackTransition != null) {
            ackTransition.stop();
        }
        ackReceivedLbl.setVisible(false);
        handshakeLbl.setVisible(false);
        ackCircle.setTranslateX(0.0);
        ackCircle.setTranslateY(0.0);
        h1Reached = false;
        r1Reached = false;
        r2Reached = false;
        animationTimer.stop();
        messageField.clear();
    }
    
    
    /*
     * This method takes in a message to be send across the network and builds
     * the ethernet frame off of it.
     */
    public String convertStringToEthernetFrame(String message) {
        EthernetFrame frame = new EthernetFrame(destMAC, sourceMAC, Utility.convertStringToBinary(message));
        return frame.getEthernetFrame();
    }
    
    // </editor-fold>

    // <editor-fold desc="FXML methods">
    @FXML
    private void playPressed(ActionEvent event) {
        paused = false;
        if (paused == false && animationStarted) {
            pauseButton.setDisable(false);
            playButton.setDisable(true);
            ackTransition.play();
            ballTransition.play();
        }
    }

    @FXML
    private void pausePressed(ActionEvent event) {
        paused = true;
        if (paused && animationStarted) {
            playButton.setDisable(false);
            pauseButton.setDisable(true);
            ackTransition.pause();
            ballTransition.pause();
        }
    }

    @FXML
    private void resetPressed(ActionEvent event) {
        this.resetScreen();
    }

    @FXML
    private void enterPressed(KeyEvent event) {
        if (event.getCode().equals(KeyCode.ENTER)) {
            if (messageField.getText().isEmpty()) {
                warningLabel.setVisible(true);
            } else {
                this.animationStarted = true;
                sendButton.setDisable(true);
                pauseButton.setDisable(false);
                warningLabel.setVisible(false);
                messageField.setDisable(true);
                message = messageField.getText();
                ethernetFrame = this.convertStringToEthernetFrame(message);
                displayArea.setText("Message: " + message);
                this.moveBall();
            }
        }
    }

    @FXML
    private void sendPressed(ActionEvent event) {
        if (messageField.getText().isEmpty()) {
            warningLabel.setVisible(true);
        } else {
            this.animationStarted = true;
            sendButton.setDisable(true);
            pauseButton.setDisable(false);
            warningLabel.setVisible(false);
            messageField.setDisable(true);
            message = messageField.getText();
            ethernetFrame = this.convertStringToEthernetFrame(message);
            displayArea.setText("Message: " + message);
            this.moveBall();
        }
    }
    // </editor-fold>
}
