package main.java.program;

import com.jfoenix.controls.JFXButton;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.animation.AnimationTimer;
import javafx.animation.PathTransition;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.control.Label;
import javafx.scene.control.Tab;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Polyline;
import javafx.scene.shape.Rectangle;
import javafx.util.Duration;
import main.java.physical_layer.NetworkSignal;
import main.java.utility.Utility;

/**
 * This class is the FXML document controller for assignment 1. It contains many
 * of the various dependency injections and the general code for how the
 * animation works.
 *
 * @author Logan Stanfield
 * @since 09/16/17
 * @version 1.0
 */
public class Assignment1Controller implements Initializable {

    // <editor-fold desc="FXML Variables.">
    // A large list of variables that reference the FXML file (dependancy injection).
    @FXML
    private ImageView model;
    @FXML
    private Circle ball;
    @FXML
    private TextField messageField;
    @FXML
    private Rectangle rect2;
    @FXML
    private Rectangle rect1;
    @FXML
    private Rectangle rect3;
    @FXML
    private Rectangle rect4;
    @FXML
    private Rectangle rect5;
    @FXML
    private Rectangle rect6;
    @FXML
    private Rectangle rect7;
    @FXML
    private Rectangle rect8;
    @FXML
    private Rectangle rect9;
    @FXML
    private Rectangle rect10;
    @FXML
    private Rectangle rect11;
    @FXML
    private Rectangle rect12;
    @FXML
    private Rectangle rect13;
    @FXML
    private Rectangle rect14;
    @FXML
    private JFXButton playButton;
    @FXML
    private JFXButton pauseButton;
    @FXML
    private JFXButton resetButton;
    @FXML
    private Label warningLabel;
    @FXML
    private JFXButton sendBtn;
    @FXML
    private Label receivedLabel;
    @FXML
    private TextArea displayArea;
    @FXML
    private Tab tab1;
    @FXML
    private Tab tab2;
    @FXML
    private Tab tab3;
    @FXML
    private LineChart<?, ?> signalChart;
    @FXML
    private NumberAxis yAxis;
    @FXML
    private NumberAxis xAxis;
    @FXML
    private Label schemeLabel;

    // </editor-fold>
    // <editor-fold desc="Non-JavaFX related variables.">
    // Non-JavaFX related variables.
    private final double leftXBounds = 139.0;
    private final double rightXBounds = 839.0;
    private final double r1Bounds = 356.0;
    private final double r2Bounds = 616.0;
    private PathTransition path;
    private boolean paused;
    private String message;
    private boolean animationStarted = false;
    private final Color defaultRectangleColor = Color.web("#AED6F1");
    private final Color reachedRectangleColor = Color.web("#F5B7B1");
    private boolean h1Reached = false;
    private boolean r1Reached = false;
    private boolean r2Reached = false;
    private boolean h2Reached = false;

    // </editor-fold>
    // <editor-fold desc="Initializer.">
    // This method initializes the class. Takes the place of a constructor.
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        this.xAxis.setAutoRanging(true);
        this.yAxis.setAutoRanging(true);
        this.signalChart.setCreateSymbols(false);
        // Changes the color of the line chart.
        this.signalChart.setStyle("CHART_COLOR_1: #FF7043");
    }

    // </editor-fold>
    //<editor-fold desc="Button handlers.">
    //
    @FXML
    private void playPressed(ActionEvent event) {
        // Begins the move ball translations.
        paused = false;
        if (paused == false && animationStarted) {
            ball.setFill(Color.web("#1FBB00"));
            playButton.setDisable(true);
            pauseButton.setDisable(false);
            path.play();
        }
    }

    // This method handles whenever the paused button is pressed.
    @FXML
    private void pausePressed(ActionEvent event) {
        paused = true;
        if (paused && animationStarted) {
            pauseButton.setDisable(true);
            playButton.setDisable(false);
            path.pause();
            ball.setFill(Color.web("#f4d03f"));
        }
    }

    // This method handles whenver the reset button has been pressed.
    @FXML
    private void resetPressed(ActionEvent event) {
        resetScreen();
    }

    // This methodhandles whenever the enter key is pressed over the text field.
    @FXML
    private void messageEntered(KeyEvent event) {
        // Check to see if enter has been pressed while the text box is selected.
        if (event.getCode().equals(KeyCode.ENTER)) {
            if (messageField.getText().isEmpty()) {
                warningLabel.setVisible(true);
            } else {
                animationStarted = true;
                sendBtn.setDisable(true);
                ball.setFill(Color.web("#1FBB00"));
                this.moveBall();
                messageField.setDisable(true);
                warningLabel.setVisible(false);
                message = messageField.getText();
                displayArea.setText("Message: " + message);
                messageField.clear();
            }
        }
    }

    @FXML
    private void ballClicked(MouseEvent event) {
        System.out.println("You clicked the ball!!!!!");
    }

    // This method handles whenever the send message button is pressed.
    @FXML
    private void sendPressed(ActionEvent event) {
        if (messageField.getText().isEmpty()) {
            warningLabel.setVisible(true);
        } else {
            animationStarted = true;
            sendBtn.setDisable(true);
            ball.setFill(Color.web("#1FBB00"));
            this.moveBall();
            messageField.setDisable(true);
            warningLabel.setVisible(false);
            message = messageField.getText();
            displayArea.setText("Message: " + message);
            messageField.clear();
        }
    }

    // </editor-fold>
    // <editor-fold desc="Helper methods.">

    /*
     * This method moves ball along the coordinated provided in the polyline shape.
     * The ball first starts moving down a long
     */
    public void moveBall() {

        // Polyline properties.
        Polyline polyline = new Polyline();
        polyline.getPoints().addAll(new Double[]{
            // x and y values for each point on the polyline.
            0.0, 0.0,
            0.0, 325.0,
            700.0, 325.0,
            700.0, 20.0
        });

        // Animation timer properties.
        AnimationTimer animationTimer = new AnimationTimer() {
            @Override
            public void handle(long time) {

                // Get a more accurate position of the ball since the AnimationCounter can be off.
                double ball_x, ball_y;
                ball_x = ball.getTranslateX() + ball.getLayoutX();
                ball_y = ball.getTranslateY() + ball.getLayoutY();
                // System.out.println("X: " + ball_x + " Y: " + ball_y);
                // Check for every rectangle to see if the ball is in bounds.
                if (this.inRectangleBounds(ball_x, ball_y, rect1, leftXBounds)) {
                    rect1.setFill(reachedRectangleColor);
                } else if (this.inRectangleBounds(ball_x, ball_y, rect2, leftXBounds)) {
                    rect2.setFill(reachedRectangleColor);
                } else if (this.inRectangleBounds(ball_x, ball_y, rect3, leftXBounds)) {
                    rect3.setFill(reachedRectangleColor);
                } else if (this.inRectangleBounds(ball_x, ball_y, rect4, leftXBounds)) {
                    rect4.setFill(reachedRectangleColor);
                } else if (this.inRectangleBounds(ball_x, ball_y, rect5, leftXBounds)) {
                    rect5.setFill(reachedRectangleColor);
                } else if (this.inRectangleBounds(ball_x, ball_y, rect6, leftXBounds)) {
                    rect6.setFill(reachedRectangleColor);
                } else if (this.inRectangleBounds(ball_x, ball_y, rect7, leftXBounds)) {
                    if (!h1Reached) {
                        displayArea.setText("Message: " + message + "\n\n" + "Data: " + Utility.convertStringToBinary(message));
                        NetworkSignal.drawDigitalSignal(Utility.convertStringToBinary(message), signalChart);
                        h1Reached = true;
                        schemeLabel.setText("NRZ-L Coding Scheme:");
                    }
                    rect7.setFill(reachedRectangleColor);
                } else if (this.inRectangleBounds(ball_x, ball_y, rect8, rightXBounds)) {
                    if (!h2Reached) {
                        displayArea.setText("Message: " + message + "\n\n" + "Data: " + Utility.convertStringToBinary(message));
                        signalChart.getData().clear();
                        schemeLabel.setText("Bitstream conversion:");
                    }
                    rect8.setFill(reachedRectangleColor);
                } else if (this.inRectangleBounds(ball_x, ball_y, rect9, rightXBounds)) {
                    rect9.setFill(reachedRectangleColor);
                } else if (this.inRectangleBounds(ball_x, ball_y, rect10, rightXBounds)) {
                    rect10.setFill(reachedRectangleColor);
                } else if (this.inRectangleBounds(ball_x, ball_y, rect11, rightXBounds)) {
                    rect11.setFill(reachedRectangleColor);
                } else if (this.inRectangleBounds(ball_x, ball_y, rect12, rightXBounds)) {
                    rect12.setFill(reachedRectangleColor);
                } else if (this.inRectangleBounds(ball_x, ball_y, rect13, rightXBounds)) {
                    rect13.setFill(reachedRectangleColor);
                } else if (this.inRectangleBounds(ball_x, ball_y, rect14, rightXBounds)) {
                    rect14.setFill(reachedRectangleColor);
                    receivedLabel.setVisible(true);
                    playButton.setDisable(true);
                    pauseButton.setDisable(true);
                } else if (ball_x > r1Bounds && ball_x < r1Bounds + 10) {
                    if (!r1Reached) {
                        NetworkSignal.drawAnalogSignal(Utility.convertStringToBinary(message), signalChart, 15);
                        schemeLabel.setText("Binary ASK:");
                        r1Reached = true;
                    }
                } else if (ball_x > r2Bounds && ball_x < r2Bounds + 10) {
                    if (!r2Reached) {
                        NetworkSignal.drawDigitalSignal(Utility.convertStringToBinary(message), signalChart);
                        schemeLabel.setText("NRZ-L Coding Scheme:");
                        r2Reached = true;
                    }
                }
            }

            // Quick helper method to check to see if the ball is in bounds of the rectangle.
            // This method is also not the most effecient it could be. This is a rough way of handling this.
            public boolean inRectangleBounds(double ball_x, double ball_y, Rectangle rect, double xBounds) {
                if (ball_y >= rect.getLayoutY()
                        && ball_x == xBounds
                        && ball_y <= (rect.getLayoutY() + rect.heightProperty().doubleValue())) {
                    return true;
                }

                return false;
            }
        };

        pauseButton.setDisable(false);

        // Start the animation timer.
        // Call this straight away to begin the timer with a zero value. 
        animationTimer.start();
        animationTimer.handle(0);

        // Set up the path transition using the polyline.
        path = new PathTransition(Duration.seconds(20), polyline, ball);
        path.setCycleCount(1);
        path.play();
    }

    /* This method resets the contents of the screen. It puts the ball back in
     * the starting position and resets all of the rectangle colors.
     */
    public void resetScreen() {
        if (path != null) {
            path.stop();
        }
        this.rect1.setFill(defaultRectangleColor);
        this.rect2.setFill(defaultRectangleColor);
        this.rect3.setFill(defaultRectangleColor);
        this.rect4.setFill(defaultRectangleColor);
        this.rect5.setFill(defaultRectangleColor);
        this.rect6.setFill(defaultRectangleColor);
        this.rect7.setFill(defaultRectangleColor);
        this.rect8.setFill(defaultRectangleColor);
        this.rect9.setFill(defaultRectangleColor);
        this.rect10.setFill(defaultRectangleColor);
        this.rect11.setFill(defaultRectangleColor);
        this.rect12.setFill(defaultRectangleColor);
        this.rect13.setFill(defaultRectangleColor);
        this.rect14.setFill(defaultRectangleColor);
        this.warningLabel.setVisible(false);
        this.messageField.setDisable(false);
        this.sendBtn.setDisable(false);
        this.ball.setTranslateX(0);
        this.ball.setTranslateY(0);
        this.receivedLabel.setVisible(false);
        this.displayArea.clear();
        this.playButton.setDisable(true);
        this.ball.setFill(Color.web("#ff7000"));
        this.animationStarted = false;
        this.signalChart.getData().clear();
        this.r1Reached = false;
        this.r2Reached = false;
        this.h1Reached = false;
        this.h2Reached = false;
        this.schemeLabel.setText("");
    }
    // </editor-fold>
}
