/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main.java.program;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * This is the Main class of the project. It first loads up the Assignment 1
 * tab.
 *
 * @author: Logan Stanfield
 * @since: 09/14/17
 * @version: 1.0
 */
public class Main extends Application {

    @Override
    public void start(Stage stage) throws Exception {

        // Set the root of the scene to be Assingment 1
        Parent root = FXMLLoader.load(getClass().getResource("Assignment1.fxml"));

        Scene scene = new Scene(root);

        // Sets the title of the window.
        stage.setTitle("CSC 567 SET-2 Project (Network Simulator)");
        stage.setResizable(false);
        stage.setScene(scene);
        stage.show();
    }

    /**
     * @param args the command line arguments.
     */
    public static void main(String[] args) {
        launch(args);
    }
}
