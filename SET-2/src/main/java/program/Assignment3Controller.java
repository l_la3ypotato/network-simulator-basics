package main.java.program;

import com.jfoenix.controls.JFXButton;
import java.net.URL;
import java.util.ArrayList;
import java.util.Random;
import java.util.ResourceBundle;
import javafx.animation.AnimationTimer;
import javafx.animation.Interpolator;
import javafx.animation.ParallelTransition;
import javafx.animation.PathTransition;
import javafx.animation.SequentialTransition;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Polyline;
import javafx.scene.shape.Rectangle;
import javafx.util.Duration;
import main.java.data_link_layer.ethernet_frame.EthernetFrame;
import main.java.network_layer.graph.Dijkstra;
import main.java.network_layer.graph.Graph;
import main.java.network_layer.ipv4.IPv4Datagram;
import main.java.physical_layer.NetworkSignal;
import main.java.utility.Utility;

/**
 * This class is the FXML document controller for assignment 3. It contains many
 * of the various dependency injections and the general code for how the
 * animation works.
 *
 * @author Logan Stanfield
 * @since 11/10/17
 * @version 1.0
 */
public class Assignment3Controller implements Initializable {

    // <editor-fold desc="Variable declarations.">
    private final Color defaultRectangleColor = Color.web("#A3E4D7");
    private final Color reachedRectangleColor = Color.web("#B7FFB8");
    private final double leftXBounds = 95.0;
    private final double rightXBounds = 915.0;
    private final double r1Bounds = 260.0;
    private final double r2Bounds = 735.0;
    private boolean h1Reached = false;
    private boolean r1Reached = false;
    private boolean r2Reached = false;
    private boolean dataLinkLayerReached = false;
    private boolean networkLayerReached = false;
    private SequentialTransition redBallTransition;
    private SequentialTransition blueBallTransition;
    private ParallelTransition p;
    private boolean paused = false;
    private String message;
    private String ethernetFrame;
    private String ipDatagram;
    private boolean animationStarted = false;
    private AnimationTimer animationTimer;
    private final String sourceMAC = "D792680FC864";
    private final String destMAC = "E6F7D259A03B";
    
    // </editor-fold>

    // <editor-fold desc="Debug variables.">
    /**
     * DEBUG MODE: Set to "ON" for debug mode and anything else for non-debug
     * mode.
     */
    String dijkstraRedDebugMode = "OFF";
    String dijkstraBlueDebugMode = "OFF";
    String redBallCoordinateDebugMode = "OFF";
    String blueBallCoordinateDebugMode = "OFF";

    // </editor-fold>
    
    // <editor-fold desc="JavaFX dependency injection variables.">
    @FXML
    private JFXButton playButton;
    @FXML
    private JFXButton pauseButton;
    @FXML
    private JFXButton resetButton;
    @FXML
    private TextField messageField;
    @FXML
    private Rectangle rect1;
    @FXML
    private Rectangle rect2;
    @FXML
    private Rectangle rect3;
    @FXML
    private Rectangle rect4;
    @FXML
    private Rectangle rect5;
    @FXML
    private Rectangle rect6;
    @FXML
    private Rectangle rect7;
    @FXML
    private Rectangle rect8;
    @FXML
    private Rectangle rect9;
    @FXML
    private Rectangle rect10;
    @FXML
    private Rectangle rect11;
    @FXML
    private Rectangle rect12;
    @FXML
    private Rectangle rect13;
    @FXML
    private Rectangle rect14;
    @FXML
    private Circle redBall;
    @FXML
    private Label v0v1RedWeight;
    @FXML
    private Label v0v5RedWeight;
    @FXML
    private Label v1v3RedWeight;
    @FXML
    private Label v3v5RedWeight;
    @FXML
    private Label v1v2RedWeight;
    @FXML
    private Label v3v4RedWeight;
    @FXML
    private Label v5v6RedWeight;
    @FXML
    private Label v2v4RedWeight;
    @FXML
    private Label v4v6RedWeight;
    @FXML
    private Label v2v7RedWeight;
    @FXML
    private Label v6v7RedWeight;
    @FXML
    private JFXButton sendButton;
    @FXML
    private Label warningLabel;
    @FXML
    private TextArea displayArea;
    @FXML
    private Label v0v1BlueWeight;
    @FXML
    private Circle blueBall;
    @FXML
    private Label v0v5BlueWeight;
    @FXML
    private Label v1v3BlueWeight;
    @FXML
    private Label v3v5BlueWeight;
    @FXML
    private Label v5v6BlueWeight;
    @FXML
    private Label v3v4BlueWeight;
    @FXML
    private Label v1v2BlueWeight;
    @FXML
    private Label v2v4BlueWeight;
    @FXML
    private Label v4v6BlueWeight;
    @FXML
    private Label v6v7BlueWeight;
    @FXML
    private Label v2v7BlueWeight;
    @FXML
    private Label receivedLabel;
    @FXML
    private LineChart<?, ?> signalChart;
    @FXML
    private NumberAxis yAxis;
    @FXML
    private NumberAxis xAxis;
    @FXML
    private Label schemeLabel;
    // </editor-fold>

    // <editor-fold desc="Initialization">
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // Set random numbers to each of the labels from 1 - 7.
        this.setRandomLabelValues(7);
        xAxis.setAutoRanging(true);
        yAxis.setAutoRanging(true);
        signalChart.setCreateSymbols(false);
        signalChart.setStyle("CHART_COLOR_1: #00BCD4");
    }
    
    // </editor-fold>

    // <editor-fold desc="Helper methods">
    /**
     *
     * @param bound
     * @return random String number ranging from 0 - bound (inclusively).
     */
    public String getRandomNumber(int bound) {
        Random r = new Random();
        return Integer.toString(r.nextInt(bound) + 1);
    }

    /**
     * This method sets a random number to the
     *
     * @param bound
     */
    public void setRandomLabelValues(int bound) {
        Random r = new Random();
        v0v1RedWeight.setText(this.getRandomNumber(bound));
        v0v5RedWeight.setText(this.getRandomNumber(bound));
        v1v2RedWeight.setText(this.getRandomNumber(bound));
        v1v3RedWeight.setText(this.getRandomNumber(bound));
        v2v4RedWeight.setText(this.getRandomNumber(bound));
        v2v7RedWeight.setText(this.getRandomNumber(bound));
        v3v4RedWeight.setText(this.getRandomNumber(bound));
        v3v5RedWeight.setText(this.getRandomNumber(bound));
        v4v6RedWeight.setText(this.getRandomNumber(bound));
        v5v6RedWeight.setText(this.getRandomNumber(bound));
        v6v7RedWeight.setText(this.getRandomNumber(bound));
        v0v1BlueWeight.setText(this.getRandomNumber(bound));
        v0v5BlueWeight.setText(this.getRandomNumber(bound));
        v1v2BlueWeight.setText(this.getRandomNumber(bound));
        v1v3BlueWeight.setText(this.getRandomNumber(bound));
        v2v4BlueWeight.setText(this.getRandomNumber(bound));
        v2v7BlueWeight.setText(this.getRandomNumber(bound));
        v3v4BlueWeight.setText(this.getRandomNumber(bound));
        v3v5BlueWeight.setText(this.getRandomNumber(bound));
        v4v6BlueWeight.setText(this.getRandomNumber(bound));
        v5v6BlueWeight.setText(this.getRandomNumber(bound));
        v6v7BlueWeight.setText(this.getRandomNumber(bound));
    }

    /**
     * This method resets the contents of the screen.
     */
    public void resetScreen() {
        rect1.setFill(defaultRectangleColor);
        rect2.setFill(defaultRectangleColor);
        rect3.setFill(defaultRectangleColor);
        rect4.setFill(defaultRectangleColor);
        rect5.setFill(defaultRectangleColor);
        rect6.setFill(defaultRectangleColor);
        rect7.setFill(defaultRectangleColor);
        rect8.setFill(defaultRectangleColor);
        rect9.setFill(defaultRectangleColor);
        rect10.setFill(defaultRectangleColor);
        rect11.setFill(defaultRectangleColor);
        rect12.setFill(defaultRectangleColor);
        rect13.setFill(defaultRectangleColor);
        rect14.setFill(defaultRectangleColor);
        setRandomLabelValues(7);
        if (p != null) {
            p.stop();
        }
        redBall.setTranslateX(0.0);
        redBall.setTranslateY(0.0);
        blueBall.setTranslateX(0.0);
        blueBall.setTranslateY(0.0);
        messageField.setDisable(false);
        playButton.setDisable(true);
        pauseButton.setDisable(true);
        if (animationTimer != null) {
            animationTimer.stop();
        }
        sendButton.setDisable(false);
        receivedLabel.setVisible(false);
        animationStarted = false;
        h1Reached = false;
        r1Reached = false;
        r2Reached = false;
        dataLinkLayerReached = false;
        networkLayerReached = false;
        signalChart.getData().clear();
        displayArea.clear();
        messageField.clear();
    }
    
    /*
     * This method takes in a message to be send across the network and builds
     * the ethernet frame off of it.
     */
    public String convertStringToEthernetFrame(String binary) {
        EthernetFrame frame = new EthernetFrame(destMAC, sourceMAC, binary);
        return frame.getEthernetFrame();
    }

    public String convertStringToDatagram(String message) {
        String retString = "";
        IPv4Datagram ip = new IPv4Datagram("152.12.214.14.", "192.16.50.212.", message, 13, 6, "14357");
        retString = ip.getIPv4Datagram();
        return retString;
    }

    /**
     * This method animates the ball and moves is accordingly.
     */
    public void moveBall() {
        animationStarted = true;

        // Finding the shortest path for the animation of the red ball.
        Graph graph = new Graph(8);
        ArrayList<String> shortestPath = null;
        ArrayList<Double> weights = null;

        Dijkstra dijkstra = new Dijkstra();

        // All edges connected to v0
        graph.addEdge(0, 1, Integer.decode(v0v1RedWeight.getText()));
        graph.addEdge(0, 5, Integer.decode(v0v5RedWeight.getText()));

        // All edges connected to v1
        graph.addEdge(1, 0, Integer.decode(v0v1RedWeight.getText()));
        graph.addEdge(1, 3, Integer.decode(v1v3RedWeight.getText()));
        graph.addEdge(1, 2, Integer.decode(v1v2RedWeight.getText()));

        // All edges connected to v2
        graph.addEdge(2, 1, Integer.decode(v1v2RedWeight.getText()));
        graph.addEdge(2, 4, Integer.decode(v2v4RedWeight.getText()));
        graph.addEdge(2, 7, Integer.decode(v2v7RedWeight.getText()));

        // All edges connected to v3
        graph.addEdge(3, 1, Integer.decode(v1v3RedWeight.getText()));
        graph.addEdge(3, 5, Integer.decode(v3v5RedWeight.getText()));
        graph.addEdge(3, 4, Integer.decode(v3v4RedWeight.getText()));

        // All edges connected to v4
        graph.addEdge(4, 3, Integer.decode(v3v4RedWeight.getText()));
        graph.addEdge(4, 6, Integer.decode(v4v6RedWeight.getText()));
        graph.addEdge(4, 2, Integer.decode(v2v4RedWeight.getText()));

        // All edges connected to v5
        graph.addEdge(5, 0, Integer.decode(v0v5RedWeight.getText()));
        graph.addEdge(5, 3, Integer.decode(v3v5RedWeight.getText()));
        graph.addEdge(5, 6, Integer.decode(v5v6RedWeight.getText()));

        // All edges connected to v6
        graph.addEdge(6, 5, Integer.decode(v5v6RedWeight.getText()));
        graph.addEdge(6, 4, Integer.decode(v4v6RedWeight.getText()));
        graph.addEdge(6, 7, Integer.decode(v6v7RedWeight.getText()));

        // All edges connected to v7
        graph.addEdge(7, 6, Integer.decode(v6v7RedWeight.getText()));
        graph.addEdge(7, 2, Integer.decode(v2v7RedWeight.getText()));

        // Calculate Dijkstra.
        dijkstra.calculate(graph.getVertex(0));
        shortestPath = dijkstra.getPath(graph, graph.getVertex(7));
        weights = dijkstra.getWeightsList(graph, graph.getVertex(7));

        // Print the dijkstra algorithm output to the screen if debug is on.
        if (dijkstraRedDebugMode.equals("ON")) {
            dijkstra.printPath(graph);
            System.out.print("============= RED BALL =============");
            dijkstra.printWeights(graph, graph.getVertex(7));
            System.out.println("");

            System.out.print("\nTraversal Path: ");
            for (String s : shortestPath) {
                System.out.print(s + " ");
            }
            System.out.println("\n");
        }

        // Router coordinates.
        double v0_x = 167.0;
        double v0_y = 345.0;
        double v1_x = 237.0;
        double v1_y = 240.0;
        double v2_x = 577.0;
        double v2_y = 240.0;
        double v3_x = 305.0;
        double v3_y = 362.0;
        double v4_x = 505.0;
        double v4_y = 345.0;
        double v5_x = 237.0;
        double v5_y = 465.0;
        double v6_x = 577.0;
        double v6_y = 460.0;
        double v7_x = 640.0;
        double v7_y = 345.0;

        // Generate the coordinates based off of the Dijkstra shortest path route.
        Polyline firstPolyline = new Polyline();
        ArrayList<PathTransition> redTransitions = new ArrayList<PathTransition>();
        Double[] lastPosition = new Double[2];
        Polyline temp;
        int index = 0;

        firstPolyline.getPoints().addAll(new Double[]{
            0.0, 0.0,
            0.0, 345.0,
            // Move to v0 since it will always go there first.
            v0_x, v0_y
        });

        // Hold the (x,y) position of node v0 since it was the first node reached.
        lastPosition[0] = v0_x;
        lastPosition[1] = v0_y;

        PathTransition first = new PathTransition(Duration.seconds(9), firstPolyline, redBall);
        first.setInterpolator(Interpolator.LINEAR);

        redTransitions.add(first);

        /* This long chain of code builds the path of the ball along with the 
         * duration of the animation. The duration of the animation comes from
         * the randomized weights on the graph.
         */
        for (String node : shortestPath) {
            switch (node) {
                case "v1": {
                    temp = new Polyline();
                    // Set the coordinates on the polyline.
                    temp.getPoints().addAll(new Double[]{
                        lastPosition[0], lastPosition[1],
                        v1_x, v1_y
                    });

                    PathTransition edgeTraversal = new PathTransition(Duration.seconds(weights.get(index) * .75), temp, redBall);
                    edgeTraversal.setDelay(Duration.ZERO);
                    edgeTraversal.setInterpolator(Interpolator.LINEAR);
                    redTransitions.add(edgeTraversal);
                    lastPosition[0] = v1_x;
                    lastPosition[1] = v1_y;
                    index++;
                    break;
                }
                case "v2": {
                    temp = new Polyline();
                    // Set the coordinates on the polyline.
                    temp.getPoints().addAll(new Double[]{
                        lastPosition[0], lastPosition[1],
                        v2_x, v2_y
                    });

                    PathTransition edgeTraversal = new PathTransition(Duration.seconds(weights.get(index) * .75), temp, redBall);
                    edgeTraversal.setDelay(Duration.ZERO);
                    edgeTraversal.setInterpolator(Interpolator.LINEAR);
                    redTransitions.add(edgeTraversal);
                    lastPosition[0] = v2_x;
                    lastPosition[1] = v2_y;
                    index++;
                    break;
                }
                case "v3": {
                    temp = new Polyline();
                    // Set the coordinates on the polyline.
                    temp.getPoints().addAll(new Double[]{
                        lastPosition[0], lastPosition[1],
                        v3_x, v3_y
                    });

                    PathTransition edgeTraversal = new PathTransition(Duration.seconds(weights.get(index) * .75), temp, redBall);
                    edgeTraversal.setDelay(Duration.ZERO);
                    edgeTraversal.setInterpolator(Interpolator.LINEAR);
                    redTransitions.add(edgeTraversal);
                    lastPosition[0] = v3_x;
                    lastPosition[1] = v3_y;
                    index++;
                    break;
                }
                case "v4": {
                    temp = new Polyline();
                    // Set the coordinates on the polyline.
                    temp.getPoints().addAll(new Double[]{
                        lastPosition[0], lastPosition[1],
                        v4_x, v4_y
                    });

                    PathTransition edgeTraversal = new PathTransition(Duration.seconds(weights.get(index) * .75), temp, redBall);
                    edgeTraversal.setDelay(Duration.ZERO);
                    edgeTraversal.setInterpolator(Interpolator.LINEAR);
                    redTransitions.add(edgeTraversal);
                    lastPosition[0] = v4_x;
                    lastPosition[1] = v4_y;
                    index++;
                    break;
                }
                case "v5": {
                    temp = new Polyline();
                    // Set the coordinates on the polyline.
                    temp.getPoints().addAll(new Double[]{
                        lastPosition[0], lastPosition[1],
                        v5_x, v5_y
                    });

                    PathTransition edgeTraversal = new PathTransition(Duration.seconds(weights.get(index) * .75), temp, redBall);
                    edgeTraversal.setDelay(Duration.ZERO);
                    edgeTraversal.setInterpolator(Interpolator.LINEAR);
                    redTransitions.add(edgeTraversal);
                    lastPosition[0] = v5_x;
                    lastPosition[1] = v5_y;
                    index++;
                    break;
                }
                case "v6": {
                    temp = new Polyline();
                    // Set the coordinates on the polyline.
                    temp.getPoints().addAll(new Double[]{
                        lastPosition[0], lastPosition[1],
                        v6_x, v6_y
                    });

                    PathTransition edgeTraversal = new PathTransition(Duration.seconds(weights.get(index) * .75), temp, redBall);
                    edgeTraversal.setDelay(Duration.ZERO);
                    edgeTraversal.setInterpolator(Interpolator.LINEAR);
                    redTransitions.add(edgeTraversal);
                    lastPosition[0] = v6_x;
                    lastPosition[1] = v6_y;
                    index++;
                    break;
                }
                case "v7": {
                    temp = new Polyline();
                    // Set the coordinates on the polyline.
                    temp.getPoints().addAll(new Double[]{
                        lastPosition[0], lastPosition[1],
                        v7_x, v7_y
                    });

                    PathTransition edgeTraversal = new PathTransition(Duration.seconds(weights.get(index) * .75), temp, redBall);
                    edgeTraversal.setDelay(Duration.ZERO);
                    edgeTraversal.setInterpolator(Interpolator.LINEAR);
                    redTransitions.add(edgeTraversal);
                    lastPosition[0] = v7_x;
                    lastPosition[1] = v7_y;
                    break;
                }
                default:
                    System.err.println("");
            }
        }

        // Last portion of the animation coordinates.
        Polyline lastPolyline = new Polyline();
        lastPolyline.getPoints().addAll(new Double[]{
            v7_x, v7_y,
            820.0, 345.0,
            820.0, 50.0
        });

        // Add the last transition to the sequence.
        PathTransition lastTransition = new PathTransition(Duration.seconds(9), lastPolyline, redBall);
        lastTransition.setDelay(Duration.ZERO);
        lastTransition.setInterpolator(Interpolator.LINEAR);
        redTransitions.add(lastTransition);

        // Blue ball dijkstra implementation and animation.================================================================
        graph = new Graph(8);
        shortestPath = null;
        weights = null;

        // All edges connected to v0
        graph.addEdge(0, 1, Integer.decode(v0v1BlueWeight.getText()));
        graph.addEdge(0, 5, Integer.decode(v0v5BlueWeight.getText()));

        // All edges connected to v1
        graph.addEdge(1, 0, Integer.decode(v0v1BlueWeight.getText()));
        graph.addEdge(1, 3, Integer.decode(v1v3BlueWeight.getText()));
        graph.addEdge(1, 2, Integer.decode(v1v2BlueWeight.getText()));

        // All edges connected to v2
        graph.addEdge(2, 1, Integer.decode(v1v2BlueWeight.getText()));
        graph.addEdge(2, 4, Integer.decode(v2v4BlueWeight.getText()));
        graph.addEdge(2, 7, Integer.decode(v2v7BlueWeight.getText()));

        // All edges connected to v3
        graph.addEdge(3, 1, Integer.decode(v1v3BlueWeight.getText()));
        graph.addEdge(3, 5, Integer.decode(v3v5BlueWeight.getText()));
        graph.addEdge(3, 4, Integer.decode(v3v4BlueWeight.getText()));

        // All edges connected to v4
        graph.addEdge(4, 3, Integer.decode(v3v4BlueWeight.getText()));
        graph.addEdge(4, 6, Integer.decode(v4v6BlueWeight.getText()));
        graph.addEdge(4, 2, Integer.decode(v2v4BlueWeight.getText()));

        // All edges connected to v5
        graph.addEdge(5, 0, Integer.decode(v0v5BlueWeight.getText()));
        graph.addEdge(5, 3, Integer.decode(v3v5BlueWeight.getText()));
        graph.addEdge(5, 6, Integer.decode(v5v6BlueWeight.getText()));

        // All edges connected to v6
        graph.addEdge(6, 5, Integer.decode(v5v6BlueWeight.getText()));
        graph.addEdge(6, 4, Integer.decode(v4v6BlueWeight.getText()));
        graph.addEdge(6, 7, Integer.decode(v6v7BlueWeight.getText()));

        // All edges connected to v7
        graph.addEdge(7, 6, Integer.decode(v6v7BlueWeight.getText()));
        graph.addEdge(7, 2, Integer.decode(v2v7BlueWeight.getText()));

        // Calculate Dijkstra.
        dijkstra.calculate(graph.getVertex(0));
        shortestPath = dijkstra.getPath(graph, graph.getVertex(7));
        weights = dijkstra.getWeightsList(graph, graph.getVertex(7));

        // Print the dijkstra algorithm output to the screen if debug is on.
        if (dijkstraBlueDebugMode.equals("ON")) {
            System.out.println("============= BLUE BALL =============");
            dijkstra.printPath(graph);
            System.out.print("\nBlue weights: ");
            dijkstra.printWeights(graph, graph.getVertex(7));

            System.out.print("Traversal Path: ");
            for (String s : shortestPath) {
                System.out.print(s + " ");
            }
            System.out.println("\n");
        }

        // Generate the coordinates based off of the Dijkstra shortest path route.
        firstPolyline = new Polyline();
        ArrayList<PathTransition> blueTransitions = new ArrayList<>();
        lastPosition = new Double[2];
        temp = null;
        index = 0;

        firstPolyline.getPoints().addAll(new Double[]{
            0.0, 0.0,
            0.0, 345.0,
            // Move to v0 since it will always go there first.
            v0_x, v0_y
        });

        // Hold the (x,y) position of node v0 since it was the first node reached.
        lastPosition[0] = v0_x;
        lastPosition[1] = v0_y;

        first = new PathTransition(Duration.seconds(9), firstPolyline, blueBall);
        first.setInterpolator(Interpolator.LINEAR);

        blueTransitions.add(first);

        for (String node : shortestPath) {
            switch (node) {
                case "v1": {
                    temp = new Polyline();
                    // Set the coordinates on the polyline.
                    temp.getPoints().addAll(new Double[]{
                        lastPosition[0], lastPosition[1],
                        v1_x, v1_y
                    });

                    PathTransition edgeTraversal = new PathTransition(Duration.seconds(weights.get(index) * .75), temp, blueBall);
                    edgeTraversal.setDelay(Duration.ZERO);
                    edgeTraversal.setInterpolator(Interpolator.LINEAR);
                    blueTransitions.add(edgeTraversal);
                    lastPosition[0] = v1_x;
                    lastPosition[1] = v1_y;
                    index++;
                    break;
                }
                case "v2": {
                    temp = new Polyline();
                    // Set the coordinates on the polyline.
                    temp.getPoints().addAll(new Double[]{
                        lastPosition[0], lastPosition[1],
                        v2_x, v2_y
                    });

                    PathTransition edgeTraversal = new PathTransition(Duration.seconds(weights.get(index) * .75), temp, blueBall);
                    edgeTraversal.setDelay(Duration.ZERO);
                    edgeTraversal.setInterpolator(Interpolator.LINEAR);
                    blueTransitions.add(edgeTraversal);
                    lastPosition[0] = v2_x;
                    lastPosition[1] = v2_y;
                    index++;
                    break;
                }
                case "v3": {
                    temp = new Polyline();
                    // Set the coordinates on the polyline.
                    temp.getPoints().addAll(new Double[]{
                        lastPosition[0], lastPosition[1],
                        v3_x, v3_y
                    });

                    PathTransition edgeTraversal = new PathTransition(Duration.seconds(weights.get(index) * .75), temp, blueBall);
                    edgeTraversal.setDelay(Duration.ZERO);
                    edgeTraversal.setInterpolator(Interpolator.LINEAR);
                    blueTransitions.add(edgeTraversal);
                    lastPosition[0] = v3_x;
                    lastPosition[1] = v3_y;
                    index++;
                    break;
                }
                case "v4": {
                    temp = new Polyline();
                    // Set the coordinates on the polyline.
                    temp.getPoints().addAll(new Double[]{
                        lastPosition[0], lastPosition[1],
                        v4_x, v4_y
                    });

                    PathTransition edgeTraversal = new PathTransition(Duration.seconds(weights.get(index) * .75), temp, blueBall);
                    edgeTraversal.setDelay(Duration.ZERO);
                    edgeTraversal.setInterpolator(Interpolator.LINEAR);
                    blueTransitions.add(edgeTraversal);
                    lastPosition[0] = v4_x;
                    lastPosition[1] = v4_y;
                    index++;
                    break;
                }
                case "v5": {
                    temp = new Polyline();
                    // Set the coordinates on the polyline.
                    temp.getPoints().addAll(new Double[]{
                        lastPosition[0], lastPosition[1],
                        v5_x, v5_y
                    });

                    PathTransition edgeTraversal = new PathTransition(Duration.seconds(weights.get(index) * .75), temp, blueBall);
                    edgeTraversal.setDelay(Duration.ZERO);
                    edgeTraversal.setInterpolator(Interpolator.LINEAR);
                    blueTransitions.add(edgeTraversal);
                    lastPosition[0] = v5_x;
                    lastPosition[1] = v5_y;
                    index++;
                    break;
                }
                case "v6": {
                    temp = new Polyline();
                    // Set the coordinates on the polyline.
                    temp.getPoints().addAll(new Double[]{
                        lastPosition[0], lastPosition[1],
                        v6_x, v6_y
                    });

                    PathTransition edgeTraversal = new PathTransition(Duration.seconds(weights.get(index) * .75), temp, blueBall);
                    edgeTraversal.setDelay(Duration.ZERO);
                    edgeTraversal.setInterpolator(Interpolator.LINEAR);
                    blueTransitions.add(edgeTraversal);
                    lastPosition[0] = v6_x;
                    lastPosition[1] = v6_y;
                    index++;
                    break;
                }
                case "v7": {
                    temp = new Polyline();
                    // Set the coordinates on the polyline.
                    temp.getPoints().addAll(new Double[]{
                        lastPosition[0], lastPosition[1],
                        v7_x, v7_y
                    });

                    PathTransition edgeTraversal = new PathTransition(Duration.seconds(weights.get(index) * .75), temp, blueBall);
                    edgeTraversal.setDelay(Duration.ZERO);
                    edgeTraversal.setInterpolator(Interpolator.LINEAR);
                    blueTransitions.add(edgeTraversal);
                    lastPosition[0] = v7_x;
                    lastPosition[1] = v7_y;
                    break;
                }
                default:
                    System.err.println("");
            }
        }

        // Last portion of the animation coordinates.
        lastPolyline = new Polyline();
        lastPolyline.getPoints().addAll(new Double[]{
            v7_x, v7_y,
            820.0, 345.0,
            820.0, 50.0
        });

        // Add the last transition.
        lastTransition = new PathTransition(Duration.seconds(9), lastPolyline, blueBall);
        lastTransition.setDelay(Duration.ZERO);
        lastTransition.setInterpolator(Interpolator.LINEAR);
        blueTransitions.add(lastTransition);

        animationTimer = new AnimationTimer() {

            /* Coordinates guide:
             *
             * v0: x = 262.0; y = 408.0
             * v1: x = 322.0; y = 303.0
             * v2: x = 672.0; y = 303.0
             * v3: x = 400.0; y = 425.0
             * v4: x = 600.0; y = 408.0
             * v5: x = 322.0; y = 528.0
             * v6: x = 672.0; y = 523.0
             * v7: x = 735.0; y = 408.0
             */
            @Override
            public void handle(long now) {
                double redBallX, redBallY, blueBallX, blueBallY;
                redBallX = redBall.getTranslateX() + redBall.getLayoutX();
                redBallY = redBall.getTranslateY() + redBall.getLayoutY();
                blueBallX = blueBall.getTranslateX() + blueBall.getLayoutX();
                blueBallY = blueBall.getTranslateY() + blueBall.getLayoutY();

                if (redBallCoordinateDebugMode.equals("ON")) {
                    System.out.println("RED: X: " + redBallX + " Y: " + redBallY);
                }

                if (blueBallCoordinateDebugMode.equals("ON")) {
                    System.out.println("BLUE: X: " + redBallX + " Y: " + redBallY);
                }

                if (this.inRectangleBounds(redBallX, redBallY, rect1, leftXBounds)) {
                    rect1.setFill(reachedRectangleColor);
                }
                if (this.inRectangleBounds(redBallX, redBallY, rect2, leftXBounds)) {
                    rect2.setFill(reachedRectangleColor);
                }
                if (this.inRectangleBounds(redBallX, redBallY, rect3, leftXBounds)) {
                    rect3.setFill(reachedRectangleColor);
                }
                if (this.inRectangleBounds(redBallX, redBallY, rect4, leftXBounds)) {
                    rect4.setFill(reachedRectangleColor);
                }
                if (this.inRectangleBounds(redBallX, redBallY, rect5, leftXBounds)) {
                    rect5.setFill(reachedRectangleColor);
                    displayArea.setText("Message: " + message + "\n\n" + "IPv4 Datagram (binary): "
                        + ipDatagram);
                }
                if (this.inRectangleBounds(redBallX, redBallY, rect6, leftXBounds)) {
                    rect6.setFill(reachedRectangleColor);
                    displayArea.setText("Message: " + message + "\n\n" + "IPv4 Datagram (binary): "
                        + ipDatagram
                        + "\n\nEthernet Frame (binary): " + ethernetFrame);
                }
                if (this.inRectangleBounds(redBallX, redBallY, rect7, leftXBounds)) {
                    rect7.setFill(reachedRectangleColor);
                    if (!h1Reached) {
                        schemeLabel.setText("NRZ-L Coding Scheme:");
                        NetworkSignal.drawDigitalSignal(ethernetFrame, signalChart);
                        h1Reached = true;
                    }
                }

                if (redBallX > r1Bounds && redBallX < r1Bounds + 10) {
                    if (!r1Reached) {
                        schemeLabel.setText("Binary ASK:");
                        NetworkSignal.drawAnalogSignal(ethernetFrame, signalChart, 75);
                        r1Reached = true;
                    }
                }

                if (redBallX > r2Bounds && redBallX < r2Bounds + 10) {
                    if (!r2Reached) {
                        schemeLabel.setText("NRZ-L Coding Scheme:");
                        NetworkSignal.drawDigitalSignal(ethernetFrame, signalChart);
                        r2Reached = true;
                    }
                }

                if (this.inRectangleBounds(redBallX, redBallY, rect8, rightXBounds)
                        || this.inRectangleBounds(blueBallX, blueBallY, rect8, rightXBounds)) {
                    rect8.setFill(reachedRectangleColor);
                    pauseButton.setDisable(true);
                    receivedLabel.setVisible(true);
                }
                if (this.inRectangleBounds(redBallX, redBallY, rect9, rightXBounds)
                        || this.inRectangleBounds(blueBallX, blueBallY, rect9, rightXBounds)) {
                    rect9.setFill(reachedRectangleColor);
                }
                if (this.inRectangleBounds(redBallX, redBallY, rect10, rightXBounds)
                        || this.inRectangleBounds(blueBallX, blueBallY, rect10, rightXBounds)) {
                    rect10.setFill(reachedRectangleColor);
                }
                if (this.inRectangleBounds(redBallX, redBallY, rect11, rightXBounds)
                        || this.inRectangleBounds(blueBallX, blueBallY, rect11, rightXBounds)) {
                    rect11.setFill(reachedRectangleColor);
                }
                if (this.inRectangleBounds(redBallX, redBallY, rect12, rightXBounds)
                        || this.inRectangleBounds(blueBallX, blueBallY, rect12, rightXBounds)) {
                    rect12.setFill(reachedRectangleColor);
                    if (!networkLayerReached) {
                        displayArea.setText("Message: " + message + "\n\n" + "IPv4 Datagram received! "
                        + "\n\nEthernet frame received!");
                        networkLayerReached = true;
                    }
                }
                if (this.inRectangleBounds(redBallX, redBallY, rect13, rightXBounds)
                        || this.inRectangleBounds(blueBallX, blueBallY, rect13, rightXBounds)) {
                    rect13.setFill(reachedRectangleColor);
                    if (!dataLinkLayerReached) {
                        displayArea.setText("Message: " + message + "\n\n" + "IPv4 Datagram (binary): "
                            + ipDatagram
                            + "\n\nEthernet Frame received!");
                        dataLinkLayerReached = true;
                    }
                }
                if (this.inRectangleBounds(redBallX, redBallY, rect14, rightXBounds)
                        || this.inRectangleBounds(blueBallX, blueBallY, rect14, rightXBounds)) {
                    rect14.setFill(reachedRectangleColor);
                    signalChart.getData().clear();
                    schemeLabel.setText("Bitstream conversion:");
                }
            }

            // This method checks to see if the ball is in the bounds of the passed in rectangle.
            public boolean inRectangleBounds(double ball_x, double ball_y, Rectangle rect, double xBounds) {
                if (ball_y >= rect.getLayoutY()
                        && ball_x == xBounds
                        && ball_y <= (rect.getLayoutY() + rect.heightProperty().doubleValue())) {
                    return true;
                }

                return false;
            }
        };

        // Start the animation timer.
        animationTimer.start();
        animationTimer.handle(0);

        // Play the red ball and blue ball animations in parallel.
        redBallTransition = new SequentialTransition(redTransitions.toArray(new PathTransition[redTransitions.size()]));
        redBallTransition.setCycleCount(1);
        blueBallTransition = new SequentialTransition(blueTransitions.toArray(new PathTransition[blueTransitions.size()]));
        blueBallTransition.setCycleCount(1);

        p = new ParallelTransition(redBallTransition, blueBallTransition);
        p.setCycleCount(1);
        p.play();
    }
    
    // </editor-fold>

    // <editor-fold desc="Java-FX button methods">
    @FXML
    private void playPressed(ActionEvent event) {
        paused = false;
        if (paused == false && animationStarted) {
            pauseButton.setDisable(false);
            playButton.setDisable(true);
            p.play();
        }
    }

    @FXML
    private void pausePressed(ActionEvent event) {
        paused = false;
        if (paused == false && animationStarted) {
            playButton.setDisable(false);
            pauseButton.setDisable(true);
            p.pause();
        }
    }

    @FXML
    private void resetPressed(ActionEvent event) {
        this.resetScreen();
    }

    @FXML
    private void sendPressed(ActionEvent event) {
        if (messageField.getText().isEmpty()) {
            warningLabel.setVisible(true);
        } else {
            sendButton.setDisable(true);
            warningLabel.setVisible(false);
            pauseButton.setDisable(false);
            messageField.setDisable(true);
            message = messageField.getText();
            ethernetFrame = this.convertStringToEthernetFrame(Utility.convertStringToBinary(message));
            displayArea.setText("Message: " + message);
            this.moveBall();
        }
    }

    @FXML
    private void enterPressed(KeyEvent event) {
        if (event.getCode().equals(KeyCode.ENTER)) {
            if (messageField.getText().isEmpty()) {
                warningLabel.setVisible(true);
            } else {
                sendButton.setDisable(true);
                warningLabel.setVisible(false);
                pauseButton.setDisable(false);
                messageField.setDisable(true);
                message = messageField.getText();
                ipDatagram = this.convertStringToDatagram(message);
                ethernetFrame = this.convertStringToEthernetFrame(ipDatagram);
                displayArea.setText("Message: " + message);
                this.moveBall();
            }
        }
    }
    
    // </editor-fold>
}
