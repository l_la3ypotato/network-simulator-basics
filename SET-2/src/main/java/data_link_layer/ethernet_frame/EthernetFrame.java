package main.java.data_link_layer.ethernet_frame;

import main.java.data_link_layer.crc.CRC;
import main.java.utility.Utility;

/**
 * This class represents an Ethernet frame. It adds the physical layer header,
 * the destination MAC address, the source MAC address, the type field, the payload
 * (padded if necessary), and the CRC correction.
 *
 * @author: Logan Stanfield
 * @since: 11/21/17
 * @version: 1.0
 */
public class EthernetFrame {

    // Each of the variables represent a section of the ethernet frame.
    String physicalLayerHeader;
    String SFD;
    String destMAC;
    String sourceMAC;
    String typeField;
    String payload;
    String crc;
    String ethernetFrame;

    public EthernetFrame(String destMAC, String sourceMAC, String binary) {
        this.physicalLayerHeader = "10101010101010101010101010101010101010101010101010101010";
        this.SFD = "10101011";
        this.destMAC = destMAC;
        this.sourceMAC = sourceMAC;
        this.typeField = "0000000000000000";
        this.payload = binary;
        this.ethernetFrame = "";
        CRC crc = new CRC();
    }
    
    public String getEthernetFrame() {
        // Add the physical layer and header to the ethernet frame in binary.
        this.ethernetFrame += this.physicalLayerHeader + this.SFD;
        // System.out.println(ethernetFrame.length());
        String temp = Utility.hexToBinary(this.destMAC);
        // Add the destination MAC address to the frame in binary.
        this.ethernetFrame += temp;
        // System.out.println(ethernetFrame);
        temp = Utility.hexToBinary(this.sourceMAC);
        // Add the source MAC address to the frame in binary
        this.ethernetFrame += temp;
        // System.out.println(ethernetFrame);
        
        // Add the typeField to the frame;
        this.ethernetFrame += this.typeField;
        // System.out.println(ethernetFrame);
        //this.payload = Utility.convertStringToBinary(this.payload);
        
        // If the length of the payload is less than 46 bytes * 8 = 368 bits, then add padding with 0's.
        if (this.payload.length() < 368) {
            this.payload = Utility.addPadding(this.payload, 368 - this.payload.length(), '0');
        }
        // Add the payload with padding to the ethernet frame.
        this.ethernetFrame += this.payload;
        
        byte[] bite = this.ethernetFrame.getBytes();
        
        // Calculate the CRC based off of the current ethernet frame.
        byte[] byteArray = this.ethernetFrame.getBytes();
        CRC crc = new CRC();
        String crcStr = "";
        crc.update(byteArray);
        
        crcStr = Long.toBinaryString(crc.getValue());
        if (crcStr.length() < 32) {
            crcStr = Utility.addPadding(crcStr, 32 - crcStr.length(), '0');
        }
        this.ethernetFrame += crcStr;
        
        // System.out.println(ethernetFrame);
        return this.ethernetFrame;
    }
}