package main.java.data_link_layer.crc;

import java.util.zip.Checksum;

/**
 * 
 * @author: Logan Stanfield
 * @since: 11/22/17
 * @version: 1.0
 */
public class CRC implements Checksum {

    /**
     * The crcValue data checksum so far.
     */
    private int crcValue = 0;

    /**
     * The fast CRC table. Computed once when the CRC32 class is loaded.
     */
    private static int[] crc_table = make_crc_table();

    /**
     * Make the table for a fast CRC.
     */
    private static int[] make_crc_table() {
        // iteratively builded the CRC lookup table.
        int[] crcLookupTable = new int[256];
        for (int i = 0; i < 256; i++) {
            int crc = i;
            // Inner loop for bit shifting.
            for (int k = 8; --k >= 0;) {
                if ((crc & 1) != 0) {
                    crc = 0xedb88320 ^ (crc >>> 1);
                } else {
                    crc = crc >>> 1;
                }
            }
            crcLookupTable[i] = crc;
        }
        return crcLookupTable;
    }

    /**
     * Returns the CRC32 data checksum computed so far.
     */
    public long getValue() {
        return (long) crcValue & 0xffffffffL;
    }

    /**
     * Resets the CRC32 data checksum as if no update was ever called.
     */
    public void reset() {
        crcValue = 0;
    }

    /**
     * Updates the checksum with the int bval.
     *
     * @param bval (the byte is taken as the lower 8 bits of bval)
     */
    public void update(int bval) {
        int c = ~crcValue;
        c = crc_table[(c ^ bval) & 0xff] ^ (c >>> 8);
        crcValue = ~c;
    }

    /**
     * Adds the byte array to the data checksum.
     *
     * @param buf the buffer which contains the data
     * @param off the offset in the buffer where the data starts
     * @param len the length of the data
     */
    public void update(byte[] buf, int off, int len) {
        int c = ~crcValue;
        while (--len >= 0) {
            c = crc_table[(c ^ buf[off++]) & 0xff] ^ (c >>> 8);
        }
        crcValue = ~c;
    }

    /**
     * Adds the complete byte array to the data checksum.
     */
    public void update(byte[] buf) {
        update(buf, 0, buf.length);
    }
}