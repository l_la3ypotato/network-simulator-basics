package main.java.network_layer.ipv4;

import main.java.utility.Utility;
import static main.java.utility.Utility.addPadding;
import static main.java.utility.Utility.convertStringToBinary;

/**
 * This class represents an IPv4 datagram. It includes all of the layers in 
 * order that are fully implemented to simulate the actual contents of a datagram.
 * 
 *
 * @author: Logan Stanfield
 * @since: 11/22/17
 * @version: 1.0
 */
public class IPv4Datagram {

    String version;
    String headerLength;
    String serviceType;
    int totalLength;
    String identification;
    String flag;
    String fragmentationOffset;
    int timeToLive;
    int protocol;
    String headerChecksum;
    String sourceIP;
    String destinationIP;

    /* In this implementation of an IPv4 datagram, option is always empty since
     * the header length is always 5. I left it here for the sake of showing
     * all of the sections of a datagram, but in this instance option will never
     * be implemented.
     */
    String option;
    String data;
    String datagram;

    public IPv4Datagram(String sourceIP, String destIP, String message, int timeToLive, int protocol, String identification) {
        this.version = "1000";
        this.headerLength = "1001";
        this.serviceType = "00000000";
        this.totalLength = 0;
        this.identification = identification;
        this.flag = "000";
        this.fragmentationOffset = "0000000000000";
        this.timeToLive = timeToLive;
        this.protocol = protocol;
        this.headerChecksum = "0000000000000000";
        this.sourceIP = sourceIP;
        this.destinationIP = destIP;
        this.data = message;
        this.datagram = "";
    }

    public String getIPv4Datagram() {
        String ttlStr = "";
        String protocolStr = "";
        String totLenStr = "";
        
        identification = Integer.toBinaryString(Integer.decode(identification));
        if (identification.length() < 16) {
            identification = addPadding(identification, 16 - identification.length(), '0');
        }
        
        ttlStr = Integer.toBinaryString(timeToLive * 2);
        if (ttlStr.length() < 8) {
            ttlStr = addPadding(ttlStr, 8 - ttlStr.length(), '0');
        }
        
        protocolStr = Integer.toBinaryString(protocol);
        if (protocolStr.length() < 8) {
            protocolStr = addPadding(protocolStr, 8 - protocolStr.length(), '0');
        }
        
        sourceIP = ipToBinary(sourceIP);
        destinationIP = ipToBinary(destinationIP);
        data = convertStringToBinary(data);

        totalLength = version.length() + headerLength.length() + serviceType.length()
                + identification.length() + flag.length() + fragmentationOffset.length()
                + ttlStr.length() + protocolStr.length() + headerChecksum.length()
                + sourceIP.length() + destinationIP.length();

        totalLength = (totalLength + 16) / 8;

        /* Total length of the header. Since th option 
         * field is always blank the header will always be 20 bytes.
         */
        totalLength = totalLength + (data.length() / 8);
        totLenStr = Integer.toBinaryString(totalLength);
        if (totLenStr.length() < 16) {
            totLenStr = addPadding(totLenStr, 16 - totLenStr.length(), '0');
        }

        this.datagram = this.version + this.headerLength + this.serviceType + totLenStr
                + identification + flag + fragmentationOffset + ttlStr + protocolStr
                + headerChecksum + sourceIP + destinationIP + data;
        return datagram;
    }

    public String ipToBinary(String ipAddress) {
        String octet = "";
        String[] strArray = new String[4];
        String tempStr = "";
        String binaryRepresentation = "";
        int i = 0;

        for (char c : ipAddress.toCharArray()) {
            if (c != '.') {
                octet += c;
            } else if (c == '.') {
                strArray[i] = octet;
                octet = "";
                i++;
            }
        }

        for (String s : strArray) {
            tempStr = Integer.toBinaryString(Integer.decode(s));
            if (tempStr.length() < 8) {
                tempStr = Utility.addPadding(tempStr, 8 - tempStr.length(), '0');
            }
            binaryRepresentation += tempStr;
        }

        return binaryRepresentation;
    }
}
