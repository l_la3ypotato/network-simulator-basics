package main.java.network_layer.graph;

import java.util.*;

/**
 * This class represents the structure of a graph. The graph is an ArrayList of
 * vertices.
 * 
 * @author: Logan Stanfield
 * @since: 11/10/17
 * @version: 1.0
 */
public class Graph {
    
    // All of the vertices mapped by the graph.
    private ArrayList<Vertex> vertices;

    // Graph object Constructor
    public Graph(int numberVertices) {
        vertices = new ArrayList<Vertex>(numberVertices);
        for (int i = 0; i < numberVertices; i++) {
            vertices.add(new Vertex("v" + Integer.toString(i)));
        }
    }

    /**
     * This method adds an edge from a source to a destination edge.
     * 
     * @param src: Source vertex.
     * @param dest: Destination vertex.
     * @param weight: Weight between the given source and destination vertices. 
     */
    public void addEdge(int src, int dest, int weight) {
        Vertex s = vertices.get(src);
        Edge new_edge = new Edge(vertices.get(dest), weight);
        s.neighbours.add(new_edge);
    }

    /**
     * This method returns an array list of the vertices in the graph.
     * 
     * @return: An ArrayList of vertices.
     */
    public ArrayList<Vertex> getVertices() {
        return vertices;
    }

    /**
     * 
     * @param vert: Number for the in the graph.
     * @return The vertex at that number.
     */
    public Vertex getVertex(int vert) {
        return vertices.get(vert);
    }
}
