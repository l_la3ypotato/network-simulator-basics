package main.java.network_layer.graph;

import java.util.*;

/**
 * This class represents a Vertex. It holds the name of the vertex, the neighbors
 * of the vertex, the path from other vertices to this vertex, the weighted distance
 * from the other vertices to this vertex, 
 * 
 * @author: Logan Stanfield
 * @since: 11/10/17
 * @version: 1.0
 */
public class Vertex implements Comparable<Vertex> {

    public final String name;
    public ArrayList<Edge> neighbours;
    public LinkedList<Vertex> path;
    public double minDistance = Double.POSITIVE_INFINITY;

    /**
     * Compares this this vertex to another vertex.
     * 
     * @param other
     * @return 
     */
    public int compareTo(Vertex other) {
        return Double.compare(minDistance, other.minDistance);
    }

    /**
     * The constructor for the Vertex object.
     * 
     * @param name: Name of the vertex.
     */
    public Vertex(String name) {
        this.name = name;
        neighbours = new ArrayList<Edge>();
        path = new LinkedList<Vertex>();
    }

    /**
     * 
     * @return: Returns the name of the vertex.
     */
    public String toString() {
        return name;
    }
}
