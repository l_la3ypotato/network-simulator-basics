package main.java.network_layer.graph;

/**
 * This class represents an edge on a graph.
 * 
 * @author fr0z3n2
 * @since 11/10/17
 * @version: 1.0
 */
public class Edge {

    // The attached vertex.
    public final Vertex target;
    // The weight to the attached vertex.
    public final double weight;

    /**
     * Constructor for an Edge object.
     * 
     * @param target
     * @param weight 
     */
    public Edge(Vertex target, double weight) {
        this.target = target;
        this.weight = weight;
    }
}
