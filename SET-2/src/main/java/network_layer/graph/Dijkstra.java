package main.java.network_layer.graph;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.PriorityQueue;

/**
 * This class performs the operations of the Dijkstra algorithm which is used in
 * the Assignment3Controller to animate the process. The main method that is
 * included in this class is not intended for anything other than debugging.
 *
 * @author: Logan Stanfield
 * @since: 11/10/17
 * @version: 1.0
 */
public class Dijkstra {

    /**
     * This method is used to test the Dijkstra algorithm ran on a graph similar
     * to the one in the handout.
     *
     * @param arg
     */
    public static void main(String[] arg) {

        Dijkstra obj = new Dijkstra();

        // Create a new graph.
        Graph g = new Graph(9);

        /*
        
        Available nodes in test graph.
        A = 0
        B = 1
        C = 2
        D = 3
        E = 4
        F = 5
        G = 6
        H = 7
        I = 8
         */
        
        //All nodes connected to A
        g.addEdge(0, 1, 4);
        g.addEdge(0, 7, 8);

        //All nodes connected to B
        g.addEdge(1, 0, 4);
        g.addEdge(1, 2, 8);
        g.addEdge(1, 7, 11);

        //All nodes connected to C
        g.addEdge(2, 1, 8);
        g.addEdge(2, 8, 2);
        g.addEdge(2, 3, 7);
        g.addEdge(2, 5, 4);

        //All nodes connected to D
        g.addEdge(3, 2, 7);
        g.addEdge(3, 5, 14);
        g.addEdge(3, 4, 9);

        //All nodes connected to E
        g.addEdge(4, 3, 9);
        g.addEdge(4, 5, 10);

        //All nodes connected to F
        g.addEdge(5, 4, 10);
        g.addEdge(5, 3, 14);
        g.addEdge(5, 2, 4);
        g.addEdge(5, 6, 2);

        //All nodes connected to g
        g.addEdge(6, 5, 2);
        g.addEdge(6, 7, 1);
        g.addEdge(6, 8, 6);

        //All nodes connected to H
        g.addEdge(7, 1, 11);
        g.addEdge(7, 8, 7);
        g.addEdge(7, 0, 8);
        g.addEdge(7, 6, 1);

        //All nodes connected to I
        g.addEdge(8, 2, 2);
        g.addEdge(8, 7, 7);
        g.addEdge(8, 6, 6);

        // Calculate Dijkstra using node 0 as the starting point.
        obj.calculate(g.getVertex(0));
        System.out.println("===================================================");
        obj.printPath(g);
        System.out.println("===================================================");
        obj.getWeightsList(g, g.getVertex(1));
    }

    /**
     * This method calculates the shortest path to all of the nodes in the 
     * graph. The nodes are first set to infinity
     * 
     */
    public void calculate(Vertex source) {
        // Algo:
        // 1. Take the unvisited node with minimum weight.
        // 2. Visit all its neighbours.
        // 3. Update the distances for all the neighbours (In the Priority Queue).
        // Repeat the process till all the connected nodes are visited.

        source.minDistance = 0;
        PriorityQueue<Vertex> queue = new PriorityQueue<>();
        queue.add(source);

        while (!queue.isEmpty()) {

            Vertex u = queue.poll();

            for (Edge neighbour : u.neighbours) {

                Double newDist = u.minDistance + neighbour.weight;

                if (neighbour.target.minDistance > newDist) {
                    // Remove the node from the queue to update the distance value.
                    queue.remove(neighbour.target);
                    neighbour.target.minDistance = newDist;

                    // Take the path visited till now and add the new node.s
                    neighbour.target.path = new LinkedList<>(u.path);
                    neighbour.target.path.add(u);

                    //Reenter the node with new distance.
                    queue.add(neighbour.target);
                }
            }
        }
    }

    /**
     * This method is used for printing the minimum distances to each node in
     * the graph.
     *
     * @param Graph graph
     */
    public void printPath(Graph g) {
        // Print the minimum Distance.
        for (Vertex v : g.getVertices()) {
            System.out.print("Vertex - " + v + " , Dist - " + v.minDistance + " , Path - ");
            for (Vertex pathvert : v.path) {
                System.out.print(pathvert + " ");
            }
            System.out.println("" + v);
        }
    }

    /**
     * This method is used to obtain the shortest path that is later used in the
     * GUI portion of this project to animate the path that is taken.
     *
     * @param g
     * @param dest
     * @return an ArrayList<String> of the shortest path taken.
     */
    public ArrayList<String> getPath(Graph g, Vertex dest) {
        ArrayList<String> retList = new ArrayList<>();

        for (Vertex pathvert : dest.path) {
            retList.add(pathvert.name);
        }

        retList.add(dest.name);
        return retList;
    }
    
    /**
     * This method prints the individual weights of the shortest path.
     * 
     * @param g: The graph used to determine the path 
     * @param dest 
     */
    public void printWeights(Graph g, Vertex dest) {
        LinkedList<Vertex> pathToDest = dest.path;
        
        for (int i = 0; i < pathToDest.size() - 1; i++) {
            for (Edge e : pathToDest.get(i).neighbours) {
                if (pathToDest.get(i + 1).name.equals(e.target.name)) {
                    System.out.print(e.weight + " ");
                }
            }
        }
    }

    /**
     * 
     * @param g: The graph used to obtain the weights.
     * @param dest: The destination node on the graph.
     * @return: Returns an ArrayList with the weights of the path taken.
     */
    public ArrayList<Double> getWeightsList(Graph g, Vertex dest) {
        ArrayList<Double> weights = new ArrayList<>();
        LinkedList<Vertex> pathToDest = dest.path;
        pathToDest.add(dest);
        int index = 1;

        for (int i = 0; i < pathToDest.size() - 1; i++) {
            for (Edge e : pathToDest.get(i).neighbours) {
                if (pathToDest.get(i + 1).name.equals(e.target.name)) {
                    weights.add(e.weight);
                }
            }
        }
        
        return weights;
    }
}
