# Welcome to the Network Simulator! #

This is a network simulating program that displays many encoding/decoding practices, error-correction and detection practice, and the animation
of Dijkstra's shortest-path algorithm.

What does it do?
This program attempts to portray what happens when a packet is sent across the network. It is shown through an animated ball moving around the screen around the OSI Model.

# How do I use it? #
Read the included Project_README.pdf file for provided instructions or just run the *.jar file in the /dist directory.

# Who do I contact? #
Author: l.la3ypotato@gmail.com
		lgstanfi@uncg.edu
		
# IPv4 Datagram Structure:
![IPv4 Datagram](http://bucarotechelp.com/networking/standards/images/IPv4dg.png)

# Ethernet Frame:
![Ethernet frame](http://ecomputernotes.com/images/Format-of-the-two-types-of-Ethernet-frames.jpg)

# Dijkstra's algorithm guide:
[Dijkstra guide](https://brilliant.org/wiki/dijkstras-short-path-finder/)

![Graph](https://ds055uzetaobb.cloudfront.net/image_optimizer/9e7d1e7f0beab28be5095491b4edcb51c22f9a6b.gif)

